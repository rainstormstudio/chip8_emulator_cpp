#include "speaker.hpp"

Speaker::Speaker() {
    std::vector<sf::Int16> samples{};
    for (i32 i = 0; i < NUM_SAMPLES; i ++) {
        samples.push_back(SAMPLE_AMPLITUDE * sin(SAMPLE_FREQUENCY * (2.0f * M_PI) * i / SAMPLE_RATE));
    }
    m_buffer.loadFromSamples(samples.data(), samples.size(), 2, SAMPLE_RATE);
    m_sound.setBuffer(m_buffer);
    m_sound.setLoop(true);
}

void Speaker::play() {
    if (m_sound.getStatus() == sf::Sound::SoundSource::Stopped) {
        m_sound.play();
    }
}

void Speaker::stop() {
    if (m_sound.getStatus() == sf::SoundSource::Playing) {
        m_sound.stop();
    }
}