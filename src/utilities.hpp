#ifndef UTILITIES_HPP
#define UTILITIES_HPP

#include <cstdlib>
#include <cstddef>
#include <cstdint>
#include <ctime>
#include <cmath>
#include <iostream>
#include <exception>
#include <vector>
#include <string>
#include <memory>

using i8 = int8_t;
using i16 = int16_t;
using i32 = int32_t;
using i64 = int64_t;

using u8 = uint8_t;
using u16 = uint16_t;
using u32 = uint32_t;
using u64 = uint64_t;

using f32 = float;
using f64 = double;

#endif // UTILITIES_HPP
