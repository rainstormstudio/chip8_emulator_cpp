#include "input.hpp"

Input::Input(std::shared_ptr<sf::RenderWindow> window) :
    m_window(window) {}

void Input::update() {
    m_hasInput = false;
    while (m_window->pollEvent(m_event)) {
        if (m_event.type == sf::Event::Closed) {
            m_window->close();
        } else if (m_event.type == sf::Event::KeyPressed ||
                   m_event.type == sf::Event::KeyReleased) {
            bool pressed = (m_event.type == sf::Event::KeyPressed) ? true : false;
            m_hasInput = pressed;
            switch (m_event.key.code) {
            case sf::Keyboard::Num1: { m_keys[KEY_1] = pressed; break; }
            case sf::Keyboard::Num2: { m_keys[KEY_2] = pressed; break; }
            case sf::Keyboard::Num3: { m_keys[KEY_3] = pressed; break; }
            case sf::Keyboard::Num4: { m_keys[KEY_4] = pressed; break; }
            case sf::Keyboard::Q: { m_keys[KEY_Q] = pressed; break; }
            case sf::Keyboard::W: { m_keys[KEY_W] = pressed; break; }
            case sf::Keyboard::E: { m_keys[KEY_E] = pressed; break; }
            case sf::Keyboard::R: { m_keys[KEY_R] = pressed; break; }
            case sf::Keyboard::A: { m_keys[KEY_A] = pressed; break; }
            case sf::Keyboard::S: { m_keys[KEY_S] = pressed; break; }
            case sf::Keyboard::D: { m_keys[KEY_D] = pressed; break; }
            case sf::Keyboard::F: { m_keys[KEY_F] = pressed; break; }
            case sf::Keyboard::Z: { m_keys[KEY_Z] = pressed; break; }
            case sf::Keyboard::X: { m_keys[KEY_X] = pressed; break; }
            case sf::Keyboard::C: { m_keys[KEY_C] = pressed; break; }
            case sf::Keyboard::V: { m_keys[KEY_V] = pressed; break; }
            default: { break; }
            }
        }
    }
}
