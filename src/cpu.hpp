#ifndef CPU_HPP
#define CPU_HPP

#define MEMORY_SIZE     4096
#define REGISTER_SIZE   16
#define STACK_SIZE      16
#define FONTS_SIZE      80
#define CPU_SPEED       12
#define SPRITE_WIDTH    8
#define SPRITE_SIZE     5
#define CPU_FPS         60

#include "utilities.hpp"
#include "renderer.hpp"
#include "input.hpp"
#include "speaker.hpp"

class Cpu {
    u8  m_memory[MEMORY_SIZE]{0};  // memory

    u8  m_v[REGISTER_SIZE]{0};  // registers

    u16 m_i;  // index register
    u16 m_pc;  // program counter

    u8  m_delayTimer;
    u8  m_soundTimer;

    u16 m_stack[STACK_SIZE]{0};
    u16 m_sp;  // stack pointer

    u16 m_opcode;
    u8  m_vx;
    u8  m_vy;

    u8  m_speed;  // how many instructions per cycle

    bool m_paused;

    Renderer* m_renderer;
    Speaker* m_speaker;
    Input* m_input;
    sf::Clock m_clock;

public:
    Cpu(Renderer* renderer, Input* input, Speaker* speaker);

    void loadFontSprites();
    void loadProgram(const std::vector<u8>& program);
    void initOPTables();
    void cycle();

private:
    inline void OP_NULL();  // NULL
    inline void OP_00E0();  // CLS
    inline void OP_00EE();  // RET
    inline void OP_1nnn();  // JP addr
    inline void OP_2nnn();  // CALL addr
    inline void OP_3xkk();  // SE Vx, byte
    inline void OP_4xkk();  // SNE Vx, byte
    inline void OP_5xy0();  // SE Vx, Vy
    inline void OP_6xkk();  // LD Vx, byte
    inline void OP_7xkk();  // ADD Vx, byte
    inline void OP_8xy0();  // LD Vx, Vy
    inline void OP_8xy1();  // OR Vx, Vy
    inline void OP_8xy2();  // AND Vx, Vy
    inline void OP_8xy3();  // XOR Vx, Vy
    inline void OP_8xy4();  // ADD Vx, Vy
    inline void OP_8xy5();  // SUB Vx, Vy
    inline void OP_8xy6();  // SHR Vx {, Vy}
    inline void OP_8xy7();  // SUBN Vx, Vy
    inline void OP_8xyE();  // SHL Vx {, Vy}
    inline void OP_9xy0();  // SNE Vx, Vy
    inline void OP_Annn();  // LD I, addr
    inline void OP_Bnnn();  // JP V0, addr
    inline void OP_Cxkk();  // RND Vx, byte
    inline void OP_Dxyn();  // DRW Vx, Vy, nibble
    inline void OP_Ex9E();  // SKP Vx
    inline void OP_ExA1();  // SKNP Vx
    inline void OP_Fx07();  // LD Vx, DT
    inline void OP_Fx0A();  // LD Vx, K
    inline void OP_Fx15();  // LD DT, Vx
    inline void OP_Fx18();  // LD ST, Vx
    inline void OP_Fx1E();  // ADD I, Vx
    inline void OP_Fx29();  // LD F, Vx
    inline void OP_Fx33();  // LD B, Vx
    inline void OP_Fx55();  // LD [I], Vx
    inline void OP_Fx65();  // LD Vx, [I]

    inline void table0();
    inline void table8();
    inline void tableE();
    inline void tableF();

private:
    typedef void (Cpu::*OP_func)();
    OP_func OP_table0[0x10] { &Cpu::OP_NULL };
    OP_func OP_table8[0x10] {
        &Cpu::OP_8xy0,  &Cpu::OP_8xy1,  &Cpu::OP_8xy2,  &Cpu::OP_8xy3,
        &Cpu::OP_8xy4,  &Cpu::OP_8xy5,  &Cpu::OP_8xy6,  &Cpu::OP_8xy7,
        &Cpu::OP_NULL,  &Cpu::OP_NULL,  &Cpu::OP_NULL,  &Cpu::OP_NULL, 
        &Cpu::OP_NULL,  &Cpu::OP_NULL,  &Cpu::OP_8xyE,  &Cpu::OP_NULL
    };
    OP_func OP_tableE[0x10] { &Cpu::OP_NULL };
    OP_func OP_tableF[0x65 + 1] { &Cpu::OP_NULL };
    OP_func OP_table[0x10]  {
        &Cpu::table0,   &Cpu::OP_1nnn,  &Cpu::OP_2nnn,  &Cpu::OP_3xkk,
        &Cpu::OP_4xkk,  &Cpu::OP_5xy0,  &Cpu::OP_6xkk,  &Cpu::OP_7xkk,
        &Cpu::table8,   &Cpu::OP_9xy0,  &Cpu::OP_Annn,  &Cpu::OP_Bnnn,
        &Cpu::OP_Cxkk,  &Cpu::OP_Dxyn,  &Cpu::tableE,   &Cpu::tableF
    };

private:
    void fetch();
    void execute();
    void updateTimers();
};

void Cpu::OP_NULL() {}

void Cpu::OP_00E0() {
    m_renderer->clear();
}

void Cpu::OP_00EE() {
    m_pc = m_stack[-- m_sp];
}

void Cpu::OP_1nnn() {
    m_pc = (m_opcode & 0xFFF);
}

void Cpu::OP_2nnn() {
    m_stack[m_sp ++] = m_pc;
    m_pc = (m_opcode & 0xFFF);
}

void Cpu::OP_3xkk() {
    if (m_v[m_vx] == (m_opcode & 0xFF)) {
        m_pc += 2;
    }
}

void Cpu::OP_4xkk() {
    if (m_v[m_vx] != (m_opcode & 0xFF)) {
        m_pc += 2;
    }
}

void Cpu::OP_5xy0() {
    if (m_v[m_vx] == m_v[m_vy]) {
        m_pc += 2;
    }
}

void Cpu::OP_6xkk() {
    m_v[m_vx] = (m_opcode & 0xFF);
}

void Cpu::OP_7xkk() {
    m_v[m_vx] += (m_opcode & 0xFF);
}

void Cpu::OP_8xy0() {
    m_v[m_vx] = m_v[m_vy];
}

void Cpu::OP_8xy1() {
    m_v[m_vx] |= m_v[m_vy];
}

void Cpu::OP_8xy2() {
    m_v[m_vx] &= m_v[m_vy];
}

void Cpu::OP_8xy3() {
    m_v[m_vx] ^= m_v[m_vy];
}

void Cpu::OP_8xy4() {
    u16 sum = static_cast<u16>(m_v[m_vx]) + static_cast<u16>(m_v[m_vy]);
    m_v[0xF] = (sum > 0xFF) ? 1 : 0;
    m_v[m_vx] = static_cast<u8>(sum);
}

void Cpu::OP_8xy5() {
    m_v[0xF] = (m_v[m_vx] > m_v[m_vy]) ? 1 : 0;
    m_v[m_vx] -= m_v[m_vy];
}

void Cpu::OP_8xy6() {
    m_v[0xF] = (m_v[m_vx] & 0x1);
    m_v[m_vx] >>= 1;
}

void Cpu::OP_8xy7() {
    m_v[0xF] = (m_v[m_vy] > m_v[m_vx]) ? 1 : 0;
    m_v[m_vx] = m_v[m_vy] - m_v[m_vx];
}

void Cpu::OP_8xyE() {
    m_v[0xF] = (m_v[m_vx] & 0x80);
    m_v[m_vx] <<= 1;
}

void Cpu::OP_9xy0() {
    if (m_v[m_vx] != m_v[m_vy]) m_pc += 2;
}

void Cpu::OP_Annn() {
    m_i = (m_opcode & 0xFFF);
}

void Cpu::OP_Bnnn() {
    m_pc = (m_opcode & 0xFFF) + m_v[0];
}

void Cpu::OP_Cxkk() {
    u8 random = static_cast<u8>(rand());
    m_v[m_vx] = random & (m_opcode & 0xFF);
}

void Cpu::OP_Dxyn() {
    u8 width = SPRITE_WIDTH;
    u8 height = (m_opcode & 0xF);

    m_v[0xF] = 0;

    for (u8 row = 0; row < height; row++) {
        u8 sprite = m_memory[m_i + row];

        for (u8 col = 0; col < width; col++) {
            if ((sprite & 0x80) > 0) {
                if (m_renderer->setPixel(m_v[m_vx] + col, m_v[m_vy] + row)) {
                    m_v[0xF] = 1;
                }
            }

            sprite <<= 1;
        }
    }
}

void Cpu::OP_Ex9E() {
    if (m_input->keyPressed(m_v[m_vx])) {
        m_pc += 2;
    }
}

void Cpu::OP_ExA1() {
    if (!m_input->keyPressed(m_v[m_vx])) {
        m_pc += 2;
    }
}

void Cpu::OP_Fx07() {
    m_v[m_vx] = m_delayTimer;
}

void Cpu::OP_Fx0A() {
    m_paused = true;
    if (!m_input->hasInput()) {
        m_pc -= 2;
    }
}

void Cpu::OP_Fx15() {
    m_delayTimer = m_v[m_vx];
}

void Cpu::OP_Fx18() {
    m_soundTimer = m_v[m_vx];
}

void Cpu::OP_Fx1E() {
    m_i += m_v[m_vx];
}

void Cpu::OP_Fx29() {
    m_i = m_v[m_vx] * SPRITE_SIZE;
}

void Cpu::OP_Fx33() {
    m_memory[m_i + 0] = m_v[m_vx] / 100;
    m_memory[m_i + 1] = (m_v[m_vx] % 100) / 10;
    m_memory[m_i + 2] = m_v[m_vx] % 10;
}

void Cpu::OP_Fx55() {
    for (u8 idx = 0; idx <= m_vx; idx ++) {
        m_memory[m_i + idx] = m_v[idx];
    }
}

void Cpu::OP_Fx65() {
    for (u8 idx = 0; idx <= m_vx; idx ++) {
        m_v[idx] = m_memory[m_i + idx];
    }
}

void Cpu::table0() {
    ((*this).*(OP_table0[m_opcode & 0xF]))();
}

void Cpu::table8() {
    ((*this).*(OP_table8[m_opcode & 0xF]))();
}

void Cpu::tableE() {
    ((*this).*(OP_tableE[m_opcode & 0xF]))();
}

void Cpu::tableF() {
    ((*this).*(OP_tableF[m_opcode & 0xFF]))();
}

#endif // CPU_HPP
