#include "cpu.hpp"

Cpu::Cpu(Renderer* renderer, Input* input, Speaker* speaker) : 
    m_renderer(renderer), m_input(input), m_speaker(speaker) 
{
    m_i = 0;
    m_pc = 0x200;

    m_delayTimer = 0;
    m_soundTimer = 0;

    m_sp = 0;

    m_speed = CPU_SPEED;

    m_paused = false;

    srand(time(NULL));

    initOPTables();
}

void Cpu::loadFontSprites() {
    const u8 fonts[FONTS_SIZE] = {
        0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
        0x20, 0x60, 0x20, 0x20, 0x70, // 1
        0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
        0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
        0x90, 0x90, 0xF0, 0x10, 0x10, // 4
        0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
        0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
        0xF0, 0x10, 0x20, 0x40, 0x40, // 7
        0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
        0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
        0xF0, 0x90, 0xF0, 0x90, 0x90, // A
        0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
        0xF0, 0x80, 0x80, 0x80, 0xF0, // C
        0xE0, 0x90, 0x90, 0x90, 0xE0, // D
        0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
        0xF0, 0x80, 0xF0, 0x80, 0x80  // F
    };
    for (size_t i = 0; i < FONTS_SIZE; i ++) {
        m_memory[0x00 + i] = fonts[i];  // fonts are stored in the memory starting at 0x00
    }
}

void Cpu::loadProgram(const std::vector<u8>& program) {
    for (size_t i = 0; i < program.size(); i ++) {
        m_memory[0x200 + i] = program[i];
    }
}

void Cpu::initOPTables() {
    OP_table0[0x0]  = &Cpu::OP_00E0;
    OP_table0[0xE]  = &Cpu::OP_00EE;

    OP_tableE[0x1]  = &Cpu::OP_ExA1;
    OP_tableE[0xE]  = &Cpu::OP_Ex9E;

    OP_tableF[0x07] = &Cpu::OP_Fx07;
    OP_tableF[0x0A] = &Cpu::OP_Fx0A;
    OP_tableF[0x15] = &Cpu::OP_Fx15;
    OP_tableF[0x18] = &Cpu::OP_Fx18;
    OP_tableF[0x1E] = &Cpu::OP_Fx1E;
    OP_tableF[0x29] = &Cpu::OP_Fx29;
    OP_tableF[0x33] = &Cpu::OP_Fx33;
    OP_tableF[0x55] = &Cpu::OP_Fx55;
    OP_tableF[0x65] = &Cpu::OP_Fx65;
}

void Cpu::cycle() {
    sf::Time deltaTime = m_clock.restart();
    f32 extra = 1.0f / CPU_FPS - deltaTime.asSeconds();
    if (extra > 0.0f) {
        sf::sleep(sf::seconds(extra));
    }
    for (u8 i = 0; i < m_speed; i ++) {
        if (!m_paused) {
            fetch();
            execute();
        }
    }

    if (!m_paused) updateTimers();
    if (m_soundTimer > 0) {
        m_speaker->play();
    } else {
        m_speaker->stop();
    }
    m_renderer->render();
}

void Cpu::fetch() {
    m_opcode = (m_memory[m_pc] << 8) | (m_memory[m_pc + 1]);
    m_pc += 2;

    m_vx = (m_opcode & 0x0F00) >> 8;
    m_vy = (m_opcode & 0x00F0) >> 4;
}

void Cpu::execute() {
    ((*this).*(OP_table[(m_opcode & 0xF000) >> 12]))();
}

void Cpu::updateTimers() {
    if (m_delayTimer > 0) m_delayTimer --;
    if (m_soundTimer > 0) m_soundTimer --;
}
