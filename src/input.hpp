#ifndef INPUT_HPP
#define INPUT_HPP

#include <SFML/Graphics.hpp>

#include "utilities.hpp"

/*
Keyboard definition

Keypad       Keyboard
+-+-+-+-+    +-+-+-+-+
|1|2|3|C|    |1|2|3|4|
+-+-+-+-+    +-+-+-+-+
|4|5|6|D|    |Q|W|E|R|
+-+-+-+-+ => +-+-+-+-+
|7|8|9|E|    |A|S|D|F|
+-+-+-+-+    +-+-+-+-+
|A|0|B|F|    |Z|X|C|V|
+-+-+-+-+    +-+-+-+-+
*/

#define KEYPAD_SIZE 16 + 1

enum KeyPad {
    KEY_1 = 1,
    KEY_2,
    KEY_3,
    KEY_4,
    KEY_Q,
    KEY_W,
    KEY_E,
    KEY_R,
    KEY_A,
    KEY_S,
    KEY_D,
    KEY_F,
    KEY_Z,
    KEY_X,
    KEY_C,
    KEY_V
};

class Input {
    std::shared_ptr<sf::RenderWindow> m_window;
    sf::Event m_event;
    bool m_keys[KEYPAD_SIZE]{false};
    bool m_hasInput{false};

public:
    Input(std::shared_ptr<sf::RenderWindow> window);
    void update();
    inline bool keyPressed(u8 idx) const { return m_keys[idx]; }
    inline bool hasInput() const { return m_hasInput; }
};

#endif // INPUT_HPP
