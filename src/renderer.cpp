#include "renderer.hpp"

Renderer::Renderer(i32 scale) :
    m_screenWidth(INNER_WIDTH * scale),
    m_screenHeight(INNER_HEIGHT * scale),
    m_scale(scale)
{
    if (!sf::Shader::isAvailable()) {
        throw std::runtime_error("Shaders are not available in this machine.");
    }
    m_window = std::make_shared<sf::RenderWindow>(
        sf::VideoMode(m_screenWidth, m_screenHeight),
        TITLE, sf::Style::Titlebar | sf::Style::Close);
    m_window->setVerticalSyncEnabled(true);
    m_window->setFramerateLimit(60);

    m_screen = std::vector<bool>(INNER_WIDTH * INNER_HEIGHT);
    m_pixels = std::vector<sf::Uint8>(INNER_WIDTH * INNER_HEIGHT * 4);
    m_screenBuffer.create(INNER_WIDTH, INNER_HEIGHT);
    m_screenSprite = sf::Sprite(m_screenBuffer);
    m_screenSprite.setScale(m_scale, m_scale);
}

bool Renderer::setPixel(i32 x, i32 y) {
    if (x >= INNER_WIDTH) x -= INNER_WIDTH;
    else if (x < 0) x += INNER_WIDTH;
    if (y >= INNER_HEIGHT) y -= INNER_HEIGHT;
    else if (y < 0) y += INNER_HEIGHT;

    i32 idx = y * INNER_WIDTH + x;
    m_screen[idx] = !m_screen[idx];

    return !m_screen[idx];
}

void Renderer::clear() {
    m_window->clear();
    for (i32 y = 0; y < INNER_HEIGHT; y ++) {
        for (i32 x = 0; x < INNER_WIDTH; x ++) {
            i32 idx = y * INNER_WIDTH + x;
            m_screen[idx] = false;
        }
    }
}

void Renderer::render() {
    for (i32 y = 0; y < INNER_HEIGHT; y ++) {
        for (i32 x = 0; x < INNER_WIDTH; x ++) {
            i32 idx = y * INNER_WIDTH + x;
            switch (m_screen[idx]) {
            case true: {
                m_pixels[idx * 4 + 0] = ON_COLOR.r;
                m_pixels[idx * 4 + 1] = ON_COLOR.g;
                m_pixels[idx * 4 + 2] = ON_COLOR.b;
                m_pixels[idx * 4 + 3] = ON_COLOR.a;
                break;
            }
            case false: {
                m_pixels[idx * 4 + 0] = OFF_COLOR.r;
                m_pixels[idx * 4 + 1] = OFF_COLOR.g;
                m_pixels[idx * 4 + 2] = OFF_COLOR.b;
                m_pixels[idx * 4 + 3] = OFF_COLOR.a;
                break;
            }
            };
        }
    }

    m_screenBuffer.update(m_pixels.data());
    m_window->draw(m_screenSprite);
    m_window->display();
}
