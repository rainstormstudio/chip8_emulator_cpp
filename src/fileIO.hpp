#ifndef FILEIO_HPP
#define FILEIO_HPP

#include <fstream>

#include "utilities.hpp"

namespace FileIO {
    std::vector<u8> readFile(const std::string& fileName);
    void printProgram(const std::vector<u8>& program);
}

#endif // FILEIO_HPP
