#include "fileIO.hpp"

std::vector<u8> FileIO::readFile(const std::string &fileName) {
    std::streampos fileSize;
    std::ifstream file(fileName, std::ios::binary);

    file.seekg(0, std::ios::end);
    fileSize = file.tellg();
    file.seekg(0, std::ios::beg);

    std::vector<u8> fileData(fileSize);
    file.read(reinterpret_cast<char*>(fileData.data()), fileSize);
    return fileData;
}

void FileIO::printProgram(const std::vector<u8>& program) {
    for (size_t i = 0; i < program.size(); i ++) {
        if (i % 8 == 0) printf("\n");
        printf("%02X ", program[i]);
    }
    printf("\n");
}
