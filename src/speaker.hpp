#ifndef SPEAKER_HPP
#define SPEAKER_HPP

#include <SFML/Audio.hpp>

#include "utilities.hpp"

class Speaker {
    const i32 NUM_SAMPLES = 100;
    const f32 SAMPLE_FREQUENCY = 440.0f;
    const i32 SAMPLE_RATE = 44100;
    const i32 SAMPLE_AMPLITUDE = 10000;

    sf::SoundBuffer m_buffer;
    sf::Sound m_sound;
public:
    Speaker();
    void play();
    void stop();
};

#endif // SPEAKER_HPP