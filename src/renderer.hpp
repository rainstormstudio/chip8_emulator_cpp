#ifndef RENDERER_HPP
#define RENDERER_HPP

#include <SFML/Graphics.hpp>

#include "utilities.hpp"

class Renderer {
    const std::string TITLE = "Chip-8 Emulator";

    const sf::Color ON_COLOR = sf::Color::White;
    const sf::Color OFF_COLOR = sf::Color::Black;

    const i32 INNER_WIDTH = 64;
    const i32 INNER_HEIGHT = 32;

    i32 m_screenWidth;
    i32 m_screenHeight;
    i32 m_scale;

    std::shared_ptr<sf::RenderWindow> m_window;

    std::vector<bool> m_screen;
    std::vector<sf::Uint8> m_pixels;
    sf::Texture m_screenBuffer;
    sf::Sprite m_screenSprite;

public:
    inline std::shared_ptr<sf::RenderWindow> window() { return m_window; }

public:
    inline bool windowIsOpen() const { return m_window->isOpen(); }

public:
    Renderer(i32 scale = 1);

    bool setPixel(i32 x, i32 y);

    void clear();
    void render();
};

#endif // RENDERER_HPP
