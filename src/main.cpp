#include "utilities.hpp"
#include "renderer.hpp"
#include "input.hpp"
#include "speaker.hpp"
#include "cpu.hpp"
#include "fileIO.hpp"

i32 main(i32 argc, char *argv[]) {
    std::string romFile{""};
    i32 scale = 10;

    try {
        for (size_t i = 0; i < argc; i ++) {
            std::string arg{argv[i]};
            if (arg == "--scale") {
                if (i + 1 < argc) {
                    std::string tmp{argv[++i]};
                    scale = std::stoi(tmp);
                } else {
                    throw std::runtime_error("missing value after --scale");
                }
            } else {
                romFile = arg;
            }
        }

        Renderer renderer{scale};
        Input input{renderer.window()};
        Speaker speaker{};
        Cpu cpu{&renderer, &input, &speaker};
        cpu.loadFontSprites();
        auto program = FileIO::readFile(romFile);
        FileIO::printProgram(program);
        cpu.loadProgram(program);

        while (renderer.windowIsOpen()) {
            input.update();
            cpu.cycle();
        }

    } catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
